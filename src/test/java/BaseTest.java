import com.paulhammant.ngwebdriver.NgWebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {

    private static final String pathToDriver = "./src/test/resources/";
    protected WebDriver driver;
    protected static NgWebDriver ngDriver;

    private void setDriver() throws Exception {
        System.setProperty("webdriver.chrome.driver", pathToDriver + "chromedriver.exe");
        driver = new ChromeDriver();
    }

    @BeforeClass
    public void setUp() throws Exception {
        setDriver();
        driver.manage().window().maximize();
        driver.get("https://hello-angularjs.appspot.com/sorttablecolumn");
        ngDriver = new NgWebDriver((JavascriptExecutor) driver);
        ngDriver.waitForAngularRequestsToFinish();
    }

    @AfterClass
    public void quit() {
        driver.quit();
    }
}