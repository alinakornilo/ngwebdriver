import com.paulhammant.ngwebdriver.ByAngular;
import org.testng.annotations.Test;

@Test
public class ComponentsTest extends BaseTest {

    public void main() throws InterruptedException {
        driver.findElement(ByAngular.model("name")).sendKeys("Test Company");
        driver.findElement(ByAngular.model("employees")).sendKeys("1000");
        driver.findElement(ByAngular.model("headoffice")).sendKeys("Mysore");
        driver.findElement(ByAngular.buttonText("Submit")).click();

        Thread.sleep(2000);
        String txt = driver.findElement(ByAngular.repeater("company in companies").row(4).column("name")).getText();
        System.out.println(txt + " Added.");

        if(txt.equalsIgnoreCase("Test Company")){
            System.out.println("New Company Added. Now remove it");
            driver.findElement(ByAngular.repeater("company in companies").row(4)).findElement(ByAngular.buttonText("Remove")).click();
        }

        Thread.sleep(3000);
    }
}
